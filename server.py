from fastapi import FastAPI, Request
from fastapi.responses import HTMLResponse
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates

import uvicorn
import getfiles

app = FastAPI()
app.mount("/static", StaticFiles(directory="static"), name='static')
templates = Jinja2Templates(directory="templates")
    
@app.get("/", name='Homepage')
async def read_root(request: Request):
    df = getfiles.get_assemble()
    return templates.TemplateResponse("index.html", {"request": request})

@app.get("/assemble", name="Assemble", response_class=HTMLResponse)
async def assemble():
    return getfiles.get_assemble().to_html()

@app.get("/senat", name="Senat", response_class=HTMLResponse)
async def senat():
    return getfiles.get_senat().to_html()

@app.get("/team", name='Team')
async def read_root(request: Request):
    return templates.TemplateResponse("elements.html", {"request": request})

@app.get("/goal", name='Goal')
async def read_root(request: Request):
#    df = getfiles.get_assemble()
    return templates.TemplateResponse("index.html", {"request": request})

@app.get("/contribute", name='Contribute')
async def read_root(request: Request):
#    df = getfiles.get_assemble()
    return templates.TemplateResponse("index.html", {"request": request})
