import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.min.css'
import Button from 'react-bootstrap/Button';



async function getDataFetch(){
    const response =
      await fetch("https://dog.ceo/api/breeds/list/all",
        { headers: {'Content-Type': 'application/json'}}
      )
    console.log(await response.json())
}

class ClientApi extends Component {
    render() {
        return (
            <div>
                <Loadbutton name="Get Predictions" onclick={getDataFetch}/>
                <Loadbutton name="Import Stocks"/>
                <Loadbutton name="Update Stocks"/>
                <Loadbutton/>
             </div>   
            )
    }
}


class Loadbutton extends Component {
    constructor(props){
        super(props)
        this.state = {name : props.name || "No name given", is_loading : false, onclick : props.onclick || this.default_event}
    }

    default_event = () => {
        setTimeout(() => this.setState(state => ({ is_loading : false })).bind(this), 3000)
    }
    
    begin_event() {
        this.setState(state => ({ is_loading : true }));
        this.state.onclick();
    }

    render(){
    return <Button variant="primary" disabled={this.state.is_loading} onClick={() => this.begin_event()}> {this.state.is_loading && "toto"} { this.state.name } </Button>
    }
}





ReactDOM.render(<ClientApi/>, document.getElementById('root'));

