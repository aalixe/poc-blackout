import pandas as pd
import requests
import convert_utils
import logging

DOWNLOAD_DIR = "static/data/"

def dl_latest_assemble():
    try:
        req = requests.get("https://www.nosdeputes.fr/deputes/enmandat/csv")
        convert_utils.write_file(req, DOWNLOAD_DIR + "assembly.csv")
    except Exception as e:
        logging.error(e)
        return 1
    return 0

def dl_latest_senat():
    try:
        req = requests.get("https://www.nossenateurs.fr/senateurs/enmandat/csv")
        convert_utils.write_file(req, DOWNLOAD_DIR + "senat.csv")
    except Exception as e:
        logging.error(e)
        return 1
    return 0

def get_assemble():
    df = pd.read_csv(DOWNLOAD_DIR + "assembly.csv", sep=';')
    return df

def get_senat():
    df = pd.read_csv(DOWNLOAD_DIR + "senat.csv", sep=';')
    return df

