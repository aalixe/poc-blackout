import requests


def write_req_to_file(req, path):
    req.encoding = req.apparent_encoding
    with open(path, "w+") as fp:
        fp.write(req.text)
    return path


def df_to_list_dict(df):
    ldict = [row.to_dict()]
    return 
